/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-bitwise */
import * as iot from './sdkIoT';
import * as ble from './sdkNoble';
import * as wifi from './sdkWlanAPI';

const HEADER_LENGTH = 5;

function getCommandType(funcName: string): number {
  let commandType = 0;
  if (funcName === 'BleSetWifiConfiguration') commandType = 0x01;
  else if (funcName === 'BleGetWifiList') commandType = 0x82;
  else if (funcName === 'BleSetBleName') commandType = 0x03;
  else if (funcName === 'BleGetBleName') commandType = 0x83;
  else if (funcName === 'BleSetMdnsName') commandType = 0x04;
  else if (funcName === 'BleGetMdnsName') commandType = 0x84;
  else if (funcName === 'BleSetApName') commandType = 0x05;
  else if (funcName === 'BleGetApName') commandType = 0x85;
  else if (funcName === 'BleSetSerialNumber') commandType = 0x06;
  else if (funcName === 'BleGetSerialNumber') commandType = 0x86;
  else if (funcName === 'BleSetEditionID') commandType = 0x07;
  else if (funcName === 'BleGetEditionID') commandType = 0x87;
  else if (funcName === 'BleSetProductType') commandType = 0x08;
  else if (funcName === 'BleGetProductType') commandType = 0x88;
  else if (funcName === 'BleSetProductID') commandType = 0x09;
  else if (funcName === 'BleGetProductID') commandType = 0x89;
  else if (funcName === 'BleGetMacAddress') commandType = 0x8a;
  else if (funcName === 'BleFactoryReset') commandType = 0x0b;
  else if (funcName === 'BleSetRainmakerNodeMapping') commandType = 0x0c;
  else if (funcName === 'BleGetRainmakerNodeMapping') commandType = 0x8c;
  else if (funcName === 'BleGetWifiConnectionState') commandType = 0x8d;
  else if (funcName === 'BleSetDeviceIdentification') commandType = 0x0e;
  return commandType;
}
class rzDeviceIoT {
  constructor() {
    iot.init();
  }

  // API for Razer_IoT_SDK.dll
  registNotifyCallback = (callback: unknown) => {
    iot.registNotifyCallback(callback);
  };

  startMdnsDeviceScan = (scanningTime: number): void => {
    iot.startMdnsDeviceScan(scanningTime);
  };

  stopMdnsDeviceScan = (): void => {
    iot.stopMdnsDeviceScan();
  };

  isNetDeviceConnected = (host: string): boolean | unknown => {
    return iot.isNetDeviceConnected(host);
  };

  connectNetDevice = (
    host: string,
    connectingTime: number
  ): number | unknown => {
    return iot.connectNetDevice(host, connectingTime);
  };

  disconnectNetDevice = (host: string): number | unknown => {
    return iot.disconnectNetDevice(host);
  };

  runCmdFw25 = (
    host: string,
    funcName: string,
    payload: string,
    payloadLength: number,
    isUDP: boolean
  ): number | unknown => {
    return iot.runCmdFw25(host, funcName, payload, payloadLength, isUDP);
  };

  runCmdEsp32 = (
    host: string,
    funcName: string,
    payload: string,
    payloadLength: number
  ): number | unknown => {
    return iot.runCmdEsp32(host, funcName, payload, payloadLength);
  };

  getSDKVersion = (): string | unknown => {
    return iot.getSDKVersion();
  };

  // API for BLE
  bleStartScan = (filters: string[]): Promise<unknown> => {
    return ble.startBLEDeviceScan(filters);
  };

  bleStopScan = (): Promise<unknown> => {
    return ble.stopBLEDeviceScan();
  };

  bleConnect = (deviceId: string): Promise<unknown> => {
    return ble.connectBLEDevice(deviceId);
  };

  bleSend = (cmd: string, data: Array<number>): Promise<unknown> => {
    const totalLen = data.length + HEADER_LENGTH;
    const arr = new Array(totalLen).fill(0);
    arr[0] = 0xaa;
    arr[1] = 0;
    arr[2] = (totalLen >> 8) & 0xff;
    arr[3] = totalLen & 0xff;
    arr[4] = getCommandType(cmd);

    for (let i = 0; i < data.length; i += 1) {
      arr[HEADER_LENGTH + i] = data[i];
    }
    return ble.sendBleCommand(arr);
  };

  // API for wifi
  APStartScan = async (_arg: unknown) => {
    return wifi.APStartScan(_arg);
  };

  APConnect = (_arg: unknown) => {
    return wifi.APConnect(_arg);
  };
}

export default rzDeviceIoT;
