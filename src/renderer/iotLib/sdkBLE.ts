import { sendDLLActionToElectron } from './ipc';
import { RZ_FFI_BLE_CHANNEL } from './constants';

const { rzBridge } = window;

const FILE_NAME = 'sdkIoT';
const IOT_DLL_NAME = 'Razer_BLE_SDK.dll';
const ACTION_LOAD_DLL = 'ConfigureFFI';

const ACTION_TRANSFER_MAP = new Map();
ACTION_TRANSFER_MAP.set('startBLEDeviceScan', 'StartBLEDeviceScan');
ACTION_TRANSFER_MAP.set('pollDevice', 'PollDevice');
ACTION_TRANSFER_MAP.set('stopBLEDeviceScan', 'StopBLEDeviceScan');
ACTION_TRANSFER_MAP.set('connectBLEDevice', 'ConnectBLEDevice');
ACTION_TRANSFER_MAP.set('receiveData', 'ReceiveData');
ACTION_TRANSFER_MAP.set('RunCmdEsp32', 'RunCmdEsp32');
ACTION_TRANSFER_MAP.set('quit', 'Quit');
ACTION_TRANSFER_MAP.set('getError', 'GetError');

export const ERROR_MSG_BUF_SIZE = 1024;

/**
 * @Author: Arthur Xin
 * @description: call API of DLL that with no param
 * @param {string} action
 * @return {*}
 */
const actionNoParam = (action: string) => {
  return sendDLLActionToElectron(RZ_FFI_BLE_CHANNEL, {
    action: ACTION_TRANSFER_MAP.get(action),
  });
};

/**
 * @Author: Arthur Xin
 * @description: call API of DLL that with params
 * @param {string} action
 * @param {array} param
 * @return {*}
 */
const actionWithParam = (action: string, ...param: unknown[]) => {
  return sendDLLActionToElectron(RZ_FFI_BLE_CHANNEL, {
    action: ACTION_TRANSFER_MAP.get(action),
    payload: {
      actionArgs: [...param],
    },
  });
};

export const startBLEDeviceScan = () => {
  const FNNAME = 'startBLEDeviceScan';

  console.log(`${FNNAME}`);
  actionNoParam(FNNAME);
};

export const pollDevice = () => {
  const FNNAME = 'pollDevice';

  console.log(`${FNNAME}`);
  return actionWithParam(FNNAME, '', false);
};

export const stopBLEDeviceScan = () => {
  const FNNAME = 'stopBLEDeviceScan';

  console.log(`${FNNAME}`);
  actionNoParam(FNNAME);
};

export const connectBLEDevice = (deviceId: string): unknown => {
  const FNNAME = 'connectBLEDevice';

  console.log(`${FNNAME}`);
  return actionWithParam(FNNAME, deviceId);
};

export const receiveData = () => {
  const FNNAME = 'receiveData';

  console.log(`${FNNAME}`);
  return actionWithParam(FNNAME, 'pointer', false);
};

/**
 * @Author: Arthur Xin
 * @description: RunCmdEsp32
 * @param {*}
 * @return {*}
 */
export const runCmdEsp32 = (
  host: string,
  funcName: string,
  payload: string,
  payloadLength: number
): number | unknown => {
  const FNNAME = 'runCmdEsp32';

  console.log(`${FNNAME} host: ${host}`);
  return actionWithParam(FNNAME, host, funcName, payload, payloadLength);
};

export const quit = () => {
  const FNNAME = 'quit';

  console.log(`${FNNAME}`);
  actionNoParam(FNNAME);
};

export const getError = () => {
  const FNNAME = 'getError';

  console.log(`${FNNAME}`);
  actionWithParam(FNNAME, 'pointer');
};

/**
 * @Author: Arthur Xin
 * @description: iot dll init
 * @param {*} boolean
 * @return {*}
 */
export const init = (): boolean => {
  const FNNAME = 'int';
  const param = {
    action: ACTION_LOAD_DLL,
    payload: {
      dllPath: `${rzBridge.dllDir}\\${IOT_DLL_NAME}`,
      apiObj: {
        StartBLEDeviceScan: ['void', []],
        PollDevice: ['int', ['pointer', 'bool']],
        StopBLEDeviceScan: ['void', []],
        ConnectBLEDevice: ['bool', ['string']],
        ReceiveData: ['bool', ['pointer', 'bool']],
        RunCmdEsp32: ['int', ['string', 'pointer', 'int']],
        Quit: ['void', []],
        GetError: ['void', ['pointer']],
      },
    },
  };
  console.log(`${FILE_NAME}.${FNNAME} param: ${param}`);

  sendDLLActionToElectron(RZ_FFI_BLE_CHANNEL, param);

  return true;
};
