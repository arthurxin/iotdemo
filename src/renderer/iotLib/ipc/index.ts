import { FFIParam } from '@types';

const FILE_NAME = 'ipc_index';
const { rzBridge } = window;

export const sendDLLActionToElectron = (
  channel: string,
  data: FFIParam
): unknown => {
  const FNNAME = 'sendDLLActionToElectron';
  let res;
  try {
    res = rzBridge.doDLLAction(channel, data);
    console.log(`${FILE_NAME}.${FNNAME} res: ${res}`);
  } catch (e) {
    console.log(e);
  }

  return res;
};

export const sendWifiActionToElectron = async (
  arg: unknown
): Promise<unknown> => {
  return rzBridge.doWifiAction(arg);
};

export const sendNobleActionToElectron = async (
  arg: unknown
): Promise<unknown> => {
  return rzBridge.doNobleAction(arg);
};
