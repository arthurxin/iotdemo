/* eslint-disable prefer-object-spread */
/* eslint-disable prettier/prettier */
import { sendWifiActionToElectron } from './ipc';

export const APStartScan = async (_arg: unknown) => {
  const FNNAME = 'APStartScan';
  return sendWifiActionToElectron(Object.assign({payload: {..._arg}}, {action: FNNAME}));
};

export const APConnect = async (_arg: unknown) => {
  const FNNAME = 'APConnect';
  return sendWifiActionToElectron(Object.assign({payload: {..._arg}}, {action: FNNAME}));
};
