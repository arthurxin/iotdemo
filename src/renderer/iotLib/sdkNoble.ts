/* eslint-disable prefer-object-spread */
/* eslint-disable prettier/prettier */
import { sendNobleActionToElectron } from './ipc';


export const startBLEDeviceScan = (filters: string[]) => {
  const FNNAME = 'startBLEDeviceScan';

  return sendNobleActionToElectron(Object.assign({payload: { filters }}, {action: FNNAME}));
};


export const stopBLEDeviceScan = () => {
  const FNNAME = 'stopBLEDeviceScan';

  console.log(`${FNNAME}`);
  return sendNobleActionToElectron(Object.assign({action: FNNAME}));
};

export const connectBLEDevice = (deviceId: string) => {
  const FNNAME = 'connectBLEDevice';

  console.log(`${FNNAME}`);
  return sendNobleActionToElectron(Object.assign({payload: { deviceId }}, {action: FNNAME}));
};

export const sendBleCommand = (data: unknown) => {
  const FNNAME = 'sendBleCommand';

  console.log(`${FNNAME} ${data}`);
  return sendNobleActionToElectron(Object.assign({payload: { data }}, {action: FNNAME}));
};

