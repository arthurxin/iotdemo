import { sendDLLActionToElectron } from './ipc';
import { RZ_FFI_IOT_CHANNEL } from './constants';

const { rzBridge } = window;

const FILE_NAME = 'sdkIoT';
const IOT_DLL_NAME = 'IoTNative.dll';
const ACTION_LOAD_DLL = 'ConfigureFFI';

let hasInit = false;
let notifyCallback: unknown;

const ACTION_TRANSFER_MAP = new Map();
ACTION_TRANSFER_MAP.set('startMdnsDeviceScan', 'IOT_StartMdnsDeviceScan');
ACTION_TRANSFER_MAP.set('stopMdnsDeviceScan', 'IOT_StopMdnsDeviceScan');
ACTION_TRANSFER_MAP.set('isNetDeviceConnected', 'IOT_IsNetDeviceConnected');
ACTION_TRANSFER_MAP.set('connectNetDevice', 'IOT_ConnectNetDevice');
ACTION_TRANSFER_MAP.set('disconnectNetDevice', 'IOT_DisconnectNetDevice');
ACTION_TRANSFER_MAP.set('runCmdFw25', 'IOT_RunCmdFw25');
ACTION_TRANSFER_MAP.set('runCmdEsp32', 'IOT_RunCmdEsp32');
ACTION_TRANSFER_MAP.set('getSDKVersion', 'IOT_GetSdkVersion');

/**
 * @Author: Arthur Xin
 * @description: call API of DLL that with no param
 * @param {string} action
 * @return {*}
 */
const actionNoParam = (action: string) => {
  return sendDLLActionToElectron(RZ_FFI_IOT_CHANNEL, {
    action: ACTION_TRANSFER_MAP.get(action),
  });
};

/**
 * @Author: Arthur Xin
 * @description: call API of DLL that with params
 * @param {string} action
 * @param {array} param
 * @return {*}
 */
const actionWithParam = (action: string, ...param: unknown[]) => {
  return sendDLLActionToElectron(RZ_FFI_IOT_CHANNEL, {
    action: ACTION_TRANSFER_MAP.get(action),
    payload: {
      actionArgs: [...param],
    },
  });
};

function ffiCallback(event: unknown) {
  console.log('ffi callback', event);
  if (notifyCallback) {
    notifyCallback(event);
  }
}

/**
 * @Author: Arthur Xin
 * @description: start mDNS scan
 * @param {number} scanningTime
 * @return {*}
 */
export const startMdnsDeviceScan = (scanningTime: number): void => {
  const FNNAME = 'startMdnsDeviceScan';

  console.log(`${FNNAME} scanningTime: ${scanningTime}`);
  actionWithParam(FNNAME, scanningTime);
};

/**
 * @Author: Arthur Xin
 * @description: stop mDns scan
 * @param {*} void
 * @return {*}
 */
export const stopMdnsDeviceScan = (): void => {
  const FNNAME = 'stopMdnsDeviceScan';

  console.log(`${FNNAME}`);
  actionNoParam(FNNAME);
};

/**
 * @Author: Arthur Xin
 * @description: To check if had connect the host
 * @param {string} host
 * @return {*}
 */
export const isNetDeviceConnected = (host: string): boolean | unknown => {
  const FNNAME = 'isNetDeviceConnected';

  console.log(`${FNNAME} host: ${host}`);
  return actionWithParam(FNNAME, host);
};

/**
 * @Author: Arthur Xin
 * @description: connect device by host IP
 * @param {string} host
 * @param {number} connectingTime
 * @return {*}
 */
export const connectNetDevice = (
  host: string,
  connectingTime: number
): number | unknown => {
  const FNNAME = 'connectNetDevice';

  console.log(`${FNNAME} host: ${host} connectingTime: ${connectingTime}`);
  return actionWithParam(FNNAME, host, connectingTime);
};

/**
 * @Author: Arthur Xin
 * @description: disconnect host by host IP
 * @param {string} host
 * @return {*}
 */
export const disconnectNetDevice = (host: string): number | unknown => {
  const FNNAME = 'disconnectNetDevice';

  console.log(`${FNNAME} host: ${host}`);
  return actionWithParam(FNNAME, host);
};

/**
 * @Author: Arthur Xin
 * @description: RunCmdFw25
 * @param {*}
 * @return {*}
 */
export const runCmdFw25 = (
  host: string,
  funcName: string,
  payload: string,
  payloadLength: number,
  isUDP: boolean
): number | unknown => {
  const FNNAME = 'runCmdFw25';

  console.log(`${FNNAME} host: ${host}`);
  return actionWithParam(FNNAME, host, funcName, payload, payloadLength, isUDP);
};

/**
 * @Author: Arthur Xin
 * @description: RunCmdEsp32
 * @param {*}
 * @return {*}
 */
export const runCmdEsp32 = (
  host: string,
  funcName: string,
  payload: string,
  payloadLength: number
): number | unknown => {
  const FNNAME = 'runCmdEsp32';

  console.log(`${FNNAME} host: ${host}`);
  return actionWithParam(FNNAME, host, funcName, payload, payloadLength);
};

/**
 * @Author: Arthur Xin
 * @description: get version of SDK
 * @param {*} string
 * @return {*}
 */
export const getSDKVersion = (): string | unknown => {
  const FNNAME = 'getSDKVersion';
  return actionNoParam(FNNAME);
};

/**
 * @Author: Arthur Xin
 * @description: registNotifyCallback
 * @param {unknown} callback
 * @return {*}
 */
export const registNotifyCallback = (callback: unknown) => {
  notifyCallback = callback;
};

/**
 * @Author: Arthur Xin
 * @description: iot dll init
 * @param {*} boolean
 * @return {*}
 */
export const init = (): boolean => {
  if (hasInit) return true;
  hasInit = true;

  const FNNAME = 'int';
  const param = {
    action: ACTION_LOAD_DLL,
    payload: {
      dllPath: `${rzBridge.dllDir}\\${IOT_DLL_NAME}`,
      apiObj: {
        GetDLLVersion: ['pointer', []],
        FreeMalloc: ['void', ['pointer']],
        SetNodeFFIEvent: ['bool', ['pointer']],

        IOT_StartMdnsDeviceScan: ['void', ['int']],
        IOT_StopMdnsDeviceScan: ['void', []],
        IOT_IsNetDeviceConnected: ['bool', ['string']],
        IOT_ConnectNetDevice: ['int', ['string', 'int']],
        IOT_DisconnectNetDevice: ['int', ['string']],
        IOT_RunCmdFw25: ['int', ['string', 'string', 'string', 'int', 'bool']],
        IOT_RunCmdEsp32: ['int', ['string', 'string', 'string', 'int']],
        IOT_GetSdkVersion: ['pointer', []],
      },
    },
  };
  console.log(`${FILE_NAME}.${FNNAME} param: ${param}`);

  sendDLLActionToElectron(RZ_FFI_IOT_CHANNEL, param);

  // get sdk version
  const version = getSDKVersion();
  console.log('DLL version:', version);

  // set call back
  const data = {
    action: 'SetNodeFFIEvent',
    payload: {
      actionArgs: ffiCallback,
    },
  };
  const r = sendDLLActionToElectron(RZ_FFI_IOT_CHANNEL, data);
  console.log('set ffi callback', r);

  return true;
};
