// channel to call API of Razer_IoT_SDK.dll
export const RZ_FFI_IOT_CHANNEL = 'rz:ffi:iot:channel';

// channel to call API of Razer_BLE_SDK.dll
export const RZ_FFI_BLE_CHANNEL = 'rz:ffi:ble:channel';

// channel to call API of wlanapi.dll
export const FFI_WLANAPI_CHANNEL = 'ffi:wlanapi:channel';

// channel to call function about electron
export const RZ_ELECTRON_CHANNEL = 'rz:electron:channel';
