/* eslint-disable import/prefer-default-export */
export function parsePayload(payload: string, split = ','): number[] {
  if (payload.length === 0) return [];

  const lowerPayload = payload.toLowerCase();

  const isHex = lowerPayload.includes('0x');
  const datas = lowerPayload.split(split);

  return datas.map((data) => {
    return parseInt(data, isHex ? 16 : 10);
  });
}

export function parseHexResponse(response: string): number[] {
  if (response.length === 0 || response.length % 2 === 1) return [];

  const res = [];
  let offset = 0;
  const GAP = 2;
  const LENGTH = response.length;
  while (offset < LENGTH) {
    const s = response.slice(offset, offset + GAP);
    const i = parseInt(s, 16);
    res.push(i);
    offset += GAP;
  }

  return res;
}
