import { MemoryRouter as Router, Routes, Route } from 'react-router-dom';
import SetupWiFi from '@/components/setup-wifi';
import Dashboard from '@/components/dashboard';

const Hello = () => {
  return (
    <div>
      <SetupWiFi />
      <Dashboard />
    </div>
  );
};

export default function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Hello />} />
      </Routes>
    </Router>
  );
}
