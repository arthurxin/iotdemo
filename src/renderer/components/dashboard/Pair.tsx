import React from 'react';
import { rzDeviceIoT } from '@/iotLib';
import * as UICommon from '@/components/common/UICommon';
import { parsePayload, parseHexResponse } from '@/modules/utils';
import { 
  NetCmdTypeCode, 
  NetCommunicationStateCode, 
  CmdCommunicationStateCode, 
  ApConfigWiFiCommandMap,
  DeviceInformationCommandClass } from '@/components/common/constants';

const BTN_START_SCAN_TEXT = 'Start Scan Device';
const BTN_STOP_SCAN_TEXT = 'Stop Scan Device';

const iot = new rzDeviceIoT;
interface PairProps {
  onClick: (msg: string) => void;
}

interface IOTDevice {
  host: string;
  hostName: string;
  eid: string;
  vid: string;
  pid: string;
  mac: string;
  productType: string;
}

interface PairState {
  devices: IOTDevice[];
  selectedId: string;
  selectedDevice: unknown;

  btnScanText: string;

  commandNames: string[];
  commandSelected: string;
  payload: string;
}
class Pair extends React.Component<PairProps, PairState> {
  constructor(props: PairProps) {
    super(props);

    this.state = {
      devices: new Array<IOTDevice>(),
      selectedId: '',
      selectedDevice: undefined,

      btnScanText: BTN_START_SCAN_TEXT,

      commandNames: [],
      commandSelected: '',
      payload: '',
    }
  }

  componentDidMount() {
    iot.registNotifyCallback(this.iotNotify.bind(this));

    let names = [];
    for (let key in DeviceInformationCommandClass) {
      names.push(key);
    }
    console.log(names);

    this.setState({
      commandNames: names,
      commandSelected: names[0],
    });
  }

  parseNetCallback(cmdType: unknown, data: unknown) {
    const { CommandData, Status, CommandName } = data;

    let type = cmdType;
    if (typeof cmdType == 'string') {
      type = parseInt(cmdType);
    }

    switch(type) {
      case NetCmdTypeCode.NET_COMM_TYPE:
        if (Status == NetCommunicationStateCode.NET_DISCONNECT) {
          this.updateResult('Disconnect');
        } else if (Status != NetCommunicationStateCode.NET_SEND_COMPLETE) {
          this.updateResult('Status code:' + Status);
        }
        break;
      case NetCmdTypeCode.NET_ESP32_TYPE:
        if (CommandName == ApConfigWiFiCommandMap.SetWifiConfiguration
          && Status == CmdCommunicationStateCode.CommandSuccessful) {
            if (CommandData.length < 2) break;

            if (CommandData[0] == 1 && CommandData[1] == 0) {
              console.log('Device connected.');
            }
          }
          else if (CommandName == ApConfigWiFiCommandMap.GetWifiList
            && Status == CmdCommunicationStateCode.CommandSuccessful) {
              console.log('wifi list ', CommandData);
          }
        break;
      case NetCmdTypeCode.NET_V25_TYPE:
        console.log(`v25 commandName: ${CommandName}`);
        if (CommandName == DeviceInformationCommandClass.GetExtendedFirmwareVersion) {
          const d = parseHexResponse(CommandData);
          let s = '';
          d.forEach(byte => {
            s += byte.toString() + '.';
          })
          s = s.substring(0, s.length - 1);
          this.updateResult('FirmwareVersion:' + s);
        } else if (CommandName == DeviceInformationCommandClass.GetSerialNumber) {
          const data = parseHexResponse(CommandData);
          let sn = '';
          data.forEach(byte => {
            sn += String.fromCharCode(Number(byte));
          });
          this.updateResult('Serial Number:' + sn);
        }
        break;
    }
  }

  parseMdnsCallback(data: unknown) {
    // console.log('mdns device ', data);

    if (data.Host && data.Mac && data.HostName) {
      let devices = [...this.state.devices];
      let bExist = false;
      for(let device of devices) {
        if (device.mac === data.Mac) {
          // already exist
          bExist = true;
          break;
        }
      }

      if (!bExist) {
        console.log('new device', data);
        this.updateResult('Detect new device: ' + JSON.stringify(data));
        devices.push({
          host: data.Host,
          hostName: data.HostName,
          eid: data.EID,
          vid: data.VID,
          pid: data.PID,
          mac: data.Mac,
          productType: data.ProductType,
        });

        this.setState({
          devices,
        });
      }
    }
  }

  iotNotify(event: unknown) {
    // console.log(event);
    const { callBackType, cmdType, data } = JSON.parse(String(event));
    if (!data) return;

    const type = parseInt(callBackType);
    if (0 == type) { // callback for ble

    } else if (1 == type) { // callback for net
      this.parseNetCallback(cmdType, data);
    } else {
      this.parseMdnsCallback(data);
    }
  }

  onSelectCommand(event: { target: { value: any; }; }) {
    const { value } = event.target;
    this.setState({
      commandSelected: value,
    });
  }

  onClickSelectDevice(event: React.MouseEvent<HTMLInputElement, MouseEvent>) {
    const { id } = event.target;
    let device = this.state.devices.filter(device => {
      console.log(`compare id: ${id} host: ${device.host}`);
      return device.host === id;
    });

    if (device.length == 0) return;

    console.log(`selected device ${device}`);

    this.setState({
      selectedId: id,
      selectedDevice: device[0],
    })
  }

  onClickSendCommand() {
    const bytes = parsePayload(this.state.payload);
    console.log(`name: ${this.state.commandSelected} data: ${bytes}`);
    console.log(`selected device: ${this.state.selectedDevice.host}`);

    let device = this.state.selectedDevice;

    iot.runCmdEsp32(device.host, this.state.commandSelected, bytes.toString(), bytes.length);
  }

  onClickScan(_event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    if (this.state.btnScanText == BTN_START_SCAN_TEXT) {
      iot.startMdnsDeviceScan(-1);

      this.setState({
        btnScanText: BTN_STOP_SCAN_TEXT,
      })
    } else {
      iot.stopMdnsDeviceScan();

      this.setState({
        btnScanText: BTN_START_SCAN_TEXT,
      })
    }
  }

  onClickConnect(_event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    let ipAddress = this.state.selectedId;

    this.updateResult(`onClickConnect ipAddress: ${ipAddress}`);

    iot.connectNetDevice(ipAddress, -1);
  }

  onClickDisconnect(_event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    let ipAddress = this.state.selectedId;
    this.updateResult(`onClickDisconnect ipAddress: ${ipAddress}`);

    iot.disconnectNetDevice(ipAddress);
  }

  renderTableHeader() {
    return (
      <thead style={{border: 'solid'}}>
        <tr>
          <th></th>
          <th>Host</th>
          <th style={{width: '200px'}}>HostName</th>
          <th>ProductType/VID/PID/EID</th>
          <th>State</th>
          <th>FW Version</th>
          <th>SN</th>
          <th>Payload(Get)</th>
          <th>Command Status</th>
          <th>OTA</th>
        </tr>
      </thead>
    );
  }

  renderTableItem() {
    return (
      <tbody>
        { this.state.devices.map(device => {
          return (
            <tr>
              <td><input type="radio" name={device.host} id={device.host} onClick={event => this.onClickSelectDevice(event)}/></td>
              <td>{device.host}</td>
              <td>{device.hostName}-{device.mac}</td>
              <td>{device.productType}/{device.vid}/{device.pid}/{device.eid}</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          )
        })}
      </tbody>
    )
  }

  rendererCommandArea() {
    return (
      <div className="row">
        <hr />
        <div>Device Information Command</div>
        <div>
          <UICommon.ShowSelect
            data={this.state.commandNames}
            value={this.state.commandSelected}
            onChange={(event) => this.onSelectCommand(event)}
            name="commandName"
            desc="Commnd Name"
            showValueOnly={true}
            insertEmptyData={false}
          />
        <input 
          className="item"
          type="text" 
          placeholder="Command Payload"
          value={this.state.payload}
          onChange={(_event) => {this.setState({
            payload: _event.target.value,
          })}}/>
        <button className='item' onClick={() => this.onClickSendCommand()}>{this.state.commandSelected}</button>
        </div>
        <hr />
      </div>
    )
  }

  updateResult(msg: string) {
    this.props.onClick(msg);
  }
  
  render() {
    return (
      <div>
        <div className="row">
          <button className="item" onClick={event => this.onClickScan(event)}>{this.state.btnScanText}</button>
          <button className="item" onClick={event => this.onClickConnect(event)}>Conect</button>
          <button className="item" onClick={event => this.onClickDisconnect(event)}>Disconect</button>
        </div>
        <div className="row pair-body">
          <table style={{width: '100%'}}>
            { this.renderTableHeader() }
            { this.renderTableItem() }
          </table>
        </div>
        <div className="row command-body">
          {this.rendererCommandArea()}
        </div>
      </div>
      
    )
  }
}

export default Pair;