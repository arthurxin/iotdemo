import React from 'react';
import Pair from './Pair';
import Result from './Result';

// import { rzDeviceIoT } from '@/iotLib';

// const iotDevice = new rzDeviceIoT();

// // test
// iotDevice.startMdnsDeviceScan(10);

// // test
// iotDevice.addSdkIotCallBack(323, (data: unknown) => {
//   console.log('Dashboard data:', data);
// });

interface DashboardState {
  result: string,
}
class Dashboard extends React.Component<any, DashboardState> {
  constructor(props: any) {
    super(props);

    this.state = {
      result: ''
    }
  }

  updateResult(msg: string) {
    console.log('upate in index.tsx:', this);
    let line = `${msg}\n${this.state.result}`;
    this.setState({
      result: line,
    })
  }

  render() {
    return (
      <div>
        <hr />
        <h3>Dashboard</h3>
        <Pair onClick={(msg) => this.updateResult(msg)}></Pair>
        <Result msg={this.state.result}></Result>
      </div>
    );
  }
}

export default Dashboard;
