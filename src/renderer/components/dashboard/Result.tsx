import React from 'react';

interface ResultProps {
  msg: string,
}

class Result extends React.Component<ResultProps> {
  render() {
    return (
      <div className="row">
        <pre style={{border: "solid", height: "250px", overflow: "auto"}}>{this.props.msg}</pre>
      </div>
    )
  }
}

export default Result;