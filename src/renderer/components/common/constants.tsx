export const enumPairMode = {
  BLE: 'BLE pairing',
  AP: 'AP pairing'
}

export const DEFAULT_PAIR_MODE = 'BLE pairing';

export const ApConfigWiFiCommand = 
[
  'SetWifiConfiguration',
  'GetWifiModuleInfo',
  'GetWifiList',
  'SetMdnsName',
  'GetMdnsName',
  'GetWifiConnectionState',
  'GetMacAddress',
  'GetMaxConnectionState',
  'FactoryReset',
  'SetDeviceIdentification',
  'GetRainmakerNodeMapping',
  'SetRainmakerNodeMapping',
  'GetCloudConnectionState',
];

export const ApConfigWiFiCommandMap = 
{
  SetWifiConfiguration: 'SetWifiConfiguration',
  GetWifiModuleInfo: 'GetWifiModuleInfo',
  GetWifiList: 'GetWifiList',
  SetMdnsName: 'SetMdnsName',
  GetMdnsName: 'GetMdnsName',
  GetWifiConnectionState: 'GetWifiConnectionState',
  GetMacAddress: 'GetMacAddress',
  GetMaxConnectionState: 'GetMaxConnectionState',
  FactoryReset: 'FactoryReset',
  SetDeviceIdentification: 'SetDeviceIdentification',
  GetRainmakerNodeMapping: 'GetRainmakerNodeMapping',
  SetRainmakerNodeMapping: 'SetRainmakerNodeMapping',
  GetCloudConnectionState: 'GetCloudConnectionState',
}


export const BLEConfigWiFiCommand =
[
  'BleSetWifiConfiguration',
  'BleGetMacAddress',
  'BleGetWifiList',
  'BleSetBleName',
  'BleGetBleName',
  'BleSetApName',
  'BleGetApName',
  'BleSetMdnsName',
  'BleGetMdnsName',
  'BleSetProductType',
  'BleGetProductType',
  'BleSetProductID',
  'BleGetProductID',
  'BleSetEditionID',
  'BleGetEditionID',
  'BleSetSerialNumber',
  'BleGetSerialNumber',
  'FactoryReset',
  'BleSetDeviceIdentification',
  'BleGetCloudConnectionState',
];

export const NetCmdTypeCode =
{
    NET_COMM_TYPE: 0,
    NET_ESP32_TYPE: 1,
    NET_V25_TYPE: 2,
};

export const NetCommunicationStateCode =
{
    NET_CONNECT: 0,
    NET_CONNECT_FAIL: 1,
    NET_READ_COMPLETE: 2,
    NET_READ_FAIL: 3,
    NET_SEND_COMPLETE: 4,
    NET_SEND_FAIL: 5,
    NET_CLOSE_COMPLETE: 6,
    NET_CLOSE_FAIL: 7,
    MDNS_SCAN_START_COMPLETE: 8,
    MDNS_SCAN_START_FAIL: 9,
    MDNS_SCAN_STOP_COMPLETE: 10,
    MDNS_SCAN_STOP_FAIL: 11,
    MDNS_SCAN_FOUND: 12,
    MDNS_SCAN_FOUND_TIMEOUT: 13,
    NET_DISCONNECT: 14,
    COMMAND_NOT_FOUND: 15,
    NET_IS_CONNECTED: 16,
};

export const CmdCommunicationStateCode = 
{
  NewCommand: 0,
  CommandBusy: 1,
  CommandSuccessful: 2,
  CommandParameterError: 3,
  CommandNoResponseTimeout: 4,
  CommandNotSupport: 5,
  ProfileNotSupport: 6,
  TargetIDNotSupport: 7,
  CommandGrabControl: 8,
  CommandNotSupportCurrentDeviceMode: 9,
  CommandSyncNotify: 10,
  CommandReserved1: 11,
  CommandReserved2: 12,
  CommandReserved3: 13,
};

export const DeviceInformationCommandClass =
{
  // GetFirmwareVersion: 'GetFirmwareVersion',
  GetExtendedFirmwareVersion: 'GetExtendedFirmwareVersion',
  // GetDeviceMode: 'GetDeviceMode',
  // SetDeviceMode: 'SetDeviceMode',
  GetSerialNumber: 'GetSerialNumber',
  // GetEditionInformation: 'GetEditionInformation',
  // DeviceReset: 'DeviceReset',
  // SetGrabControl: 'SetGrabControl',
  // GetGrabControl: 'GetGrabControl',
  // GetAppMacAddress: 'GetAppMacAddress',
  // GetLightingMode: 'GetLightingMode',
  // SetLightingMode: 'SetLightingMode',
  // GetDeviceHardwareInformation: 'GetDeviceHardwareInformation',
};