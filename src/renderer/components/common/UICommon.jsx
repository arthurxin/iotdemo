import React from 'react';
import PropTypes from 'prop-types';
// import Slider, { createSliderWithTooltip } from 'rc-slider';

const SelectOption = (props) => {
  if (props.data === undefined) return <select />;

  // console.log("props.data", props);

  let optionTemplate;
  if (Array.isArray(props.data)) {
    optionTemplate = props.data.map((v, i) => (
      <option key={i} value={v}>
        {v}
      </option>
    ));
  } else {
    // enum object
    const displayData = Object.entries(props.data);
    if (props.useEnumObjectKey) {
      if (props.showValueOnly) {
        // show values only
        optionTemplate = displayData.map((v, i) => (
          <option key={i} value={v[0]}>
            {v[1]}
          </option>
        ));
      } else {
        // show key and values
        optionTemplate = displayData.map((v, i) => (
          <option key={i} value={v[0]}>
            {v[0]} - {v[1]}
          </option>
        ));
      }
    } else {
      if (props.showValueOnly) {
        // show values only
        optionTemplate = displayData.map((v, i) => (
          <option key={i} value={v[1]}>
            {v[1]}
          </option>
        ));
      } else if (props.showKeyOnly) {
        // show key only
        optionTemplate = displayData.map((v, i) => (
          <option key={i} value={v[0]}>
            {v[0]}
          </option>
        ));
      } else {
        // show key and values
        optionTemplate = displayData.map((v, i) => (
          <option key={i} value={v[1]}>
            {v[0]} - {v[1]}
          </option>
        ));
      }
    }
  }

  if (props.insertEmptyData) {
    const emptyData = [''];
    const emptyOptionTemplate = emptyData.map((v) => (
      <option key="-1" value={v}>
        {v}
      </option>
    ));

    optionTemplate.unshift(emptyOptionTemplate[0]);
    // console.log("optionTemplate", optionTemplate);
  }

  return (
    <select onChange={props.onChange} value={props.value} name={props.name}>
      {optionTemplate}
    </select>
  );
};
SelectOption.propTypes = {
  data: PropTypes.any,
  name: PropTypes.string,
  value: PropTypes.any,
  insertEmptyData: PropTypes.bool,
  showValueOnly: PropTypes.bool,
  showKeyOnly: PropTypes.bool,
  useEnumObjectKey: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
};

export const ShowSelect = (props) => {
  return (
    <span>
      <span style={{ marginRight: 5 }}>
        {props.desc}
        {props.desc !== undefined && props.desc !== '' ? ':' : ''}
      </span>
      <span style={{ marginLeft: 5 }}>
        <SelectOption {...props} />
      </span>
    </span>
  );
};
ShowSelect.propTypes = {
  desc: PropTypes.string,
  data: PropTypes.any,
  name: PropTypes.string,
  value: PropTypes.any,
  insertEmptyData: PropTypes.bool,
  showValueOnly: PropTypes.bool,
  showKeyOnly: PropTypes.bool,
  useEnumObjectKey: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
};

// const SliderWithTooltip = createSliderWithTooltip(Slider);

// export const ShowSliderWithTooltip = (props) => {
//   // 	<SliderWithTooltip
//   // 	tipFormatter={percentFormatter}
//   // 	tipProps={{ overlayClassName: 'foo' }}
//   // 	onChange={log}
//   //   />
//   return (
//     <div style={{ width: 300, margin: 10 }}>
//       <SliderWithTooltip
//         min={props.min}
//         max={props.max}
//         value={props.value}
//         step={props.step !== undefined ? props.step : 1}
//         onChange={props.onChange}
//         onAfterChange={props.onAfterChange}
//         tipFormatter={(value) => `${value}`}
//         // tipProps={{ overlayClassName: "foo" }}
//       />
//       {/* disabled={props.disabled} */}
//       <div className="rangeSlider-labels">
//         <div>{props.min}</div>
//         <div>{props.max}</div>
//       </div>
//     </div>
//   );
// };
// ShowSliderWithTooltip.propTypes = {
//   min: PropTypes.number,
//   max: PropTypes.number,
//   step: PropTypes.number,
//   value: PropTypes.number,
//   onChange: PropTypes.func.isRequired,
//   onAfterChange: PropTypes.func.isRequired,
// };

// export const ShowSliderWithTooltipSpan = (props) => {
//   // 	<SliderWithTooltip
//   // 	tipFormatter={percentFormatter}
//   // 	tipProps={{ overlayClassName: 'foo' }}
//   // 	onChange={log}
//   //   />
//   return (
//     <span style={{ width: 300, margin: 10, textAlign: 'left', display: 'inline-block' }}>
//       <SliderWithTooltip
//         min={props.min}
//         max={props.max}
//         value={props.value}
//         step={props.step !== undefined ? props.step : 1}
//         onChange={props.onChange}
//         onAfterChange={props.onAfterChange}
//         tipFormatter={(value) => `${value}`}
//         // tipProps={{ overlayClassName: "foo" }}
//       />
//       {/* disabled={props.disabled} */}
//       <div className="rangeSlider-labels">
//         <div>{props.min}</div>
//         <div>{props.max}</div>
//       </div>
//     </span>
//   );
// };
// ShowSliderWithTooltipSpan.propTypes = {
//   min: PropTypes.number,
//   max: PropTypes.number,
//   step: PropTypes.number,
//   value: PropTypes.number,
//   onChange: PropTypes.func.isRequired,
//   onAfterChange: PropTypes.func.isRequired,
// };

export const Checkbox = ({ name, onChange, checked, desc }) => {
  return (
    <label>
      {/* <input type="checkbox" name={name} id={name} checked={checked} defaultChecked={defaultChecked} onChange={onChange} /> */}
      <input type="checkbox" name={name} id={name} checked={checked} onChange={onChange} />
      <span style={{ marginRight: 10 }}>{desc}</span>
    </label>
  );
};
Checkbox.propTypes = {
  name: PropTypes.string,
  desc: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  checked: PropTypes.bool,
};

export const TextInput = (props) => (
  <span>
    {props.desc}
    <input
      type="text"
      name={props.name}
      value={props.value}
      onChange={props.onChange}
      size={props.size}
      style={props.style !== undefined ? props.style : { marginLeft: 5 }}
    />
  </span>
);
TextInput.propTypes = {
  name: PropTypes.string,
  desc: PropTypes.string,
  value: PropTypes.string,
  size: PropTypes.number,
  onChange: PropTypes.func.isRequired,
  style: PropTypes.any,
};

export const TextInputNumber = (props) => (
  <span>
    {props.desc}
    <input
      type="number"
      name={props.name}
      value={props.value}
      onChange={props.onChange}
      // size={props.size}
      min={0}
      max={0xffffffff}
      style={props.style !== undefined ? props.style : { marginLeft: 5 }}
    />
  </span>
);
TextInputNumber.propTypes = {
  name: PropTypes.string,
  desc: PropTypes.string,
  value: PropTypes.string,
  size: PropTypes.number,
  onChange: PropTypes.func.isRequired,
  style: PropTypes.any,
};

export const NumberInput = (props) => (
  <span>
    {props.desc}
    <input
      type="number"
      name={props.name}
      value={props.value}
      onChange={props.onChange}
      min={props.min}
      max={props.max}
      step={props.step}
      style={{ marginLeft: 10 }}
    />
  </span>
);
NumberInput.propTypes = {
  name: PropTypes.string,
  desc: PropTypes.string,
  value: PropTypes.number,
  min: PropTypes.number,
  max: PropTypes.number,
  step: PropTypes.number,
  onChange: PropTypes.func.isRequired,
  style: PropTypes.any,
};

export const prettifyXml = function (sourceXml) {
  const xmlDoc = new DOMParser().parseFromString(sourceXml, 'application/xml');
  const xsltDoc = new DOMParser().parseFromString(
    [
      // describes how we want to modify the XML - indent everything
      '<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform">',
      '  <xsl:strip-space elements="*"/>',
      '  <xsl:template match="para[content-style][not(text())]">', // change to just text() to strip space in text nodes
      '    <xsl:value-of select="normalize-space(.)"/>',
      '  </xsl:template>',
      '  <xsl:template match="node()|@*">',
      '    <xsl:copy><xsl:apply-templates select="node()|@*"/></xsl:copy>',
      '  </xsl:template>',
      '  <xsl:output indent="yes"/>',
      '</xsl:stylesheet>',
    ].join('\n'),
    'application/xml'
  );

  const xsltProcessor = new XSLTProcessor();
  xsltProcessor.importStylesheet(xsltDoc);
  const resultDoc = xsltProcessor.transformToDocument(xmlDoc);
  const resultXml = new XMLSerializer().serializeToString(resultDoc);
  return resultXml;
};

export const printJSON = (jsonData) => {
  if (jsonData === undefined || Object.entries(jsonData).length === 0) {
    return null;
  } else {
    return (
      <div>
        <pre style={{ fontSize: 12 }}>{JSON.stringify(jsonData, null, 2)}</pre>
      </div>
    );
  }
};

export const ShowToggle = (props) => {
  return (
    <span>
      <label className="switch">
        <input onChange={props.onChange} name={props.name} type="checkbox" checked={props.checked} data-id={props.dataId} />
        <span className="slider" />
      </label>
    </span>
  );
};
ShowToggle.propTypes = {
  name: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  style: PropTypes.object,
  checked: PropTypes.bool,
  dataId: PropTypes.any,
};

export const TextArea = (props) => (
  <span>
    {props.displayText} {props.displayText !== undefined && props.displayText !== '' ? ':' : ''}
    {/* <textarea name={props.name} value={props.value} onChange={props.onChange} style={{ marginLeft: 10, width: 600, height: 100 }} /> */}
    <textarea name={props.name} value={props.value} onChange={props.onChange} style={props.style} />
    <br />
  </span>
);
TextArea.propTypes = {
  name: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  style: PropTypes.object,
  displayText: PropTypes.string,
};
