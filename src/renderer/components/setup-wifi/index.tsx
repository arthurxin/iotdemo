import * as UICommon from '@/components/common/UICommon';
import React from 'react';
import { rzDeviceIoT } from '@/iotLib';
import { DEFAULT_PAIR_MODE, 
  enumPairMode,
  ApConfigWiFiCommand,
  BLEConfigWiFiCommand } from '@/components/common/constants';
import './index.css';

const { rzBridge } = window;
// const { ipcRenderer } = window.require('electron')

let iot = new rzDeviceIoT;

const WIFI_FILTERS = ['Razer', 'RZ', 'Jade', 'Gillian', 'Paige', 'Joanna'];

const CONNEXT_TEXT = 'Connect';
const DISCONNECT_TEXT = 'Disconnect';

const START_SCAN_TEXT = 'Start Scan';
const STOP_SCAN_TEXT = 'Stop Scan';

interface WirelessDevice {
  name: string;
  id: string;
  command: string;
  status: string;
}
interface SetupState {
  // for header
  devices: WirelessDevice[];
  deviceSelected: string;
  mode: string;

  // for footer
  wifiList: unknown[],
  wifiNames: string[],
  wifiSelected: string,
  wifiPassword: string,

  commandList: string[],
  commandSelected: string,
  commandPayload: string,

  // ui text
  scanBtnText: string,
  connectBtnText: string,
}
class SetupWifiConfig extends React.Component<any, SetupState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      // for header
      devices: new Array<WirelessDevice>(), // array of wifi or ble
      deviceSelected: '', // wifi or ble that selected
      mode: DEFAULT_PAIR_MODE,

      // for footer
      wifiList: [],
      wifiNames: [],
      wifiSelected: '',
      wifiPassword: '',

      commandList: [],
      commandSelected: '',
      commandPayload: '',

      // ui text
      scanBtnText: START_SCAN_TEXT,
      connectBtnText: CONNEXT_TEXT,
    }
  }

  componentDidMount() {
    // this.updateWifiList([]);
    this.updateWifiList([...WIFI_FILTERS]);
    this.updateCommandList(DEFAULT_PAIR_MODE);

    rzBridge.receive('rzNewBLEDevice', (_event: any, args: any) => {
      let newBle = JSON.parse(args);
      let devices = [...this.state.devices];

      for (let device of devices) {
        if (device.id == newBle.id) {
          return;
        }
      }

      devices.push({
        name: newBle.name,
        id: newBle.id,
        command:'',
        status: ''
      });
      console.log('new BLE device ', devices);
      this.setState({
        devices,
      })
    });

    rzBridge.receive('rzBLECommandResult', (_event: any, args: any) => {
      console.log('command result:', args);
      console.log(typeof args);
      console.log(String.fromCharCode(...args));
    });
  }

  // select AP or BLE mode
  onSelectMode(_event: any) {
    const { value } = _event.target;
    this.setState({
      mode: value
    });

    console.log('value', value);

    // update command list
    let nextMode = value;
    this.updateCommandList(nextMode);
  }

  // select wifi or BLE
  onClickSelectDevice(event: React.MouseEvent<HTMLInputElement, MouseEvent>) {
    console.log('select device', event.target);
    const { id, name } = event.target;
    console.log(name, id);
    this.setState({
      deviceSelected: id
    });
  }

  // Scan Wifi or BLE
  async onClickScan(_event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    if (this.state.scanBtnText === START_SCAN_TEXT) {
      // clear device list first
      this.setState({
        devices: [],
      });
    }

    if (this.state.mode === enumPairMode.AP) {
      let { data: networks } = await iot.APStartScan({
        filters: [...WIFI_FILTERS],
      });
      console.log('&&&rrrr', networks);

      // 设置WiFi选择列表
      let devices: WirelessDevice[] = [];
      networks.forEach((net: { ssid: any; mac: any; }) => {
        devices.push({
          name: net.ssid,
          id: net.mac,
          command:'',
          status: '',
        });
      });

      this.setState({
        devices,
      })
    } else {
      if (this.state.scanBtnText === START_SCAN_TEXT) {
        iot.bleStartScan(['Razer']).then(res => {
          console.log('Begin scan.', res);

          this.setState({
            scanBtnText: STOP_SCAN_TEXT,
          });
        })
      } else {
        // stop scan
        iot.bleStopScan().then(res => {
          console.log(`stop scan: ${res}`);
        })

        this.setState({
          scanBtnText: START_SCAN_TEXT,
        });
      }
    }
  }

  // Connect Wifi or BLE
  async onClickConnect(_event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    if (this.state.mode == enumPairMode.AP) {
      if (this.state.deviceSelected.length === 0) {
        alert('Please select device');
        return;
      }

      const ap = {
        ssid: this.state.deviceSelected,
        // password: '',
        password: 'Razer123', // for test
      }
      console.log('AP:', ap);
      
      const res = await iot.APConnect(ap);
      console.log('connect res:', res);
    } else {
      if (this.state.connectBtnText === CONNEXT_TEXT) {
        iot.bleConnect(this.state.deviceSelected).then(res => {
          console.log('Connect result:', res);
          alert('Connect device successfully');
          this.setState({
            connectBtnText: DISCONNECT_TEXT,
          })
        }).catch(res => {
          console.log('connect catch ', res);
        })
      } else {
        // code for disconnect
        // ??? wait implement

        this.setState({
          connectBtnText: CONNEXT_TEXT,
        })
      }
    }
  }

  onSelectWifi(_event: any) {
    const { value } = _event.target;
    this.setState({
      wifiSelected: value
    });
  }

  onSelectCommand(_evnet: any) {
    const { value } = _evnet.target;
    this.setState({
      commandSelected: value
    });
  }

  sendCommand(_event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    if (this.state.mode == enumPairMode.AP) {

    } else {
      let arr = new Array();
      let commandSelected = this.state.commandSelected;
      let payload = this.state.commandPayload;
      if (commandSelected === 'BleSetWifiConfiguration') {
        let wifiName = this.state.wifiSelected;
        let pwd = this.state.wifiPassword;
        console.log(`${wifiName} ${pwd}`);

        arr.push(wifiName.length);
        arr.push(pwd.length);
        for(let letter of wifiName) {
          arr.push(letter.charCodeAt(0));
        }

        for(let letter of pwd) {
          arr.push(letter.charCodeAt(0));
        }
      } else if (payload.length > 0) {
        payload.split(' ').forEach(element => arr.push(Number.parseInt(element)));
      }

      console.log('send payload ', arr);

      iot.bleSend(this.state.commandSelected, arr).then(res => {
        if (this.state.commandSelected == BLEConfigWiFiCommand[0] && res[0] == 1 && res[1] == 0) {
          alert('set wifi configuration successfully');
        }
        console.log('command ', res);
      }).catch(res => {
        console.log('send error', res);
      })
    }
  }

  async updateCommandList(nextMode: string) {
    if (nextMode === enumPairMode.AP) {
      this.setState({
        commandList: ApConfigWiFiCommand,
        commandSelected: ApConfigWiFiCommand[0],
      })
    } else {
      this.setState({
        commandList: BLEConfigWiFiCommand,
        commandSelected: BLEConfigWiFiCommand[0],
      })
    }
  }

  async getWifiList(filters: Array<string>) {
    let networks = await iot.APStartScan({
      filters: [...filters],
    });
    
    return networks;
  }

  async updateWifiList(filters: Array<string> = []) {
    let { data : networks } = await this.getWifiList(filters);
    let names: any[] = [];

    console.log(networks);

    networks.forEach((net: { ssid: any; }) => {
      names.push(net.ssid);
    })

    this.setState({
      wifiList: networks,
      wifiNames: names,
    })
  }
  
  renderTableHeader() {
    return (
      <thead style={{border: 'solid'}}>
        <tr>
          <th></th>
          <th>Device Name</th>
          <th>Device Id</th>
          <th>Command Data</th>
          <th>Command Status</th>
        </tr>
      </thead>
    );
  }

  renderTableItem() {
    return (
      <tbody>
        { this.state.devices.map(device => {
          return (
            <tr>
              <td><input type="radio" name={device.name} id={device.id} onClick={event => this.onClickSelectDevice(event)}/></td>
              <td>{device.name}</td>
              <td>{device.id}</td>
              <td>{device.command}</td>
              <td>{device.status}</td>
            </tr>
          )
        })}
      </tbody>
    )
  }

  getHeader() {
    return (
      <div>
        {/* action */}
        <div className="pair-header">
          <div className="row" style={{display: "inline-block"}}>
            <UICommon.ShowSelect
              data={enumPairMode}
              value={this.state.mode}
              onChange={(event) => this.onSelectMode(event)}
              name="paring-mode"
              desc=""
              showValueOnly={true}
              insertEmptyData={false}
            />
          </div>
          <button className="item" onClick={event => this.onClickScan(event)}>{this.state.scanBtnText}</button>
          <button className="item" onClick={event => this.onClickConnect(event)}>{this.state.connectBtnText}</button>
        </div>
        {/* list */}
        <div className="pair-body">
          {/* <table style={{border: 'solid', width: '100%', minHeight: "150px", maxHeight: "200px"}}> */}
          <table style={{width: '100%'}}>
            { this.renderTableHeader() }
            { this.renderTableItem() }
          </table>
        </div>
      </div>
    )
  }

  getFooter() {
    return (
      <div className="footer">
        <div className="row">
          <UICommon.ShowSelect
            data={this.state.wifiNames}
            value={this.state.wifiSelected}
            onChange={(event) => this.onSelectWifi(event)}
            name="ssid"
            desc="SSID"
            showValueOnly={true}
            insertEmptyData={false}
          />
          <button className="item" onClick={() => this.updateWifiList()}>Refresh</button>
        </div>
        <div className="row">
          <span style={{marginRight: '12px'}}>Password:</span>
          <input
            type="text"
            name="password"
            value={this.state.wifiPassword}
            onChange={(_event) => {this.setState({
              wifiPassword: _event.target.value,
            })}}
          />
        </div>
        <div className="row">
          <span style={{marginRight: '12px'}}>Payload:</span>
          <input
            type="text"
            name="payload"
            value={this.state.commandPayload}
            onChange={(_event) => {this.setState({
              commandPayload: _event.target.value,
            })}}
          />
        </div>
        <div className="row">
          <UICommon.ShowSelect
            data={this.state.commandList}
            value={this.state.commandSelected}  
            onChange={(event) => this.onSelectCommand(event)}
            name="ssid"
            desc="Command Type"
            showValueOnly={true}
            insertEmptyData={false}
          />
          <button className="item" onClick={(_event) => this.sendCommand(_event)}>{this.state.commandSelected}</button>
        </div>
      </div>
    )
  }

  render () {
    return (
      <div style={{ fontSize: 14 }}>
        <h3>SetupWiFi</h3>
        {this.getHeader()}
        {this.getFooter()}
      </div>
    );
  }
};

export default SetupWifiConfig;