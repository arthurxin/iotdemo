import { Callback } from 'ffi-napi';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface FFIAPIs {}

interface FFIPayload {
  actionArgs?: unknown;
  dllPath?: string;
  apiObj?: FFIAPIs;
}

interface FFIParam {
  action: string;
  payload?: FFIPayload;
}

interface LIBFunc {
  SetNodeFFIEvent: Callback;
}

/// // <reference path="..." />
