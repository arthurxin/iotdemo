import { FFIParam } from '@types';
import { BRIDGE_NAME } from 'commons/jsbridge';
import { contextBridge, ipcRenderer } from 'electron';
import * as ffi from '@/modules/ffi';

contextBridge.exposeInMainWorld(BRIDGE_NAME, {
  electronVersion: process.versions.electron,
  chromeVersion: process.versions.chrome,
  nodeVersion: process.versions.node,
  appData: process.env.APPDATA,
  localAppData: process.env.LocalAppData,
  userDataDir: `${process.env.LocalAppData}\\Razer\\RazerAppEngine\\User Data`,
  dllDir: `${process.env.LocalAppData}\\Razer\\RzAppEngine\\User Data\\Apps\\Synapse`,
  rendererProcessId: process.pid,

  doDLLAction: (channel: string, arg: FFIParam): unknown => {
    const res = ffi.callDLL(channel, arg);
    return res;
  },

  doWifiAction: async (arg: unknown) => {
    return ipcRenderer.invoke('electron:wifi:channel', arg);
  },

  doNobleAction: async (arg: unknown) => {
    return ipcRenderer.invoke('electron:noble:channel', arg);
  },

  receive: (channel: string, func: unknown) => {
    // Deliberately strip event as it includes `sender`
    ipcRenderer.on(channel, (_event, args) => {
      console.log('receive event', args);
      func(_event, args);
    });
  },
});
