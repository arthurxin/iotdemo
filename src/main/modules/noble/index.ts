/* eslint-disable no-restricted-syntax */
/* eslint-disable import/prefer-default-export */
// temporary
import { BrowserWindow } from 'electron';

const noble = require('@abandonware/noble')({ extended: false });

let isBLEOpen: boolean;
let bleFilters: string[];
const devices = new Map<string, unknown>();

const TARGET_SERVICE_ID = '776db329e9634ee3bd4df3b779e8b0f8';
const TARGET_CHARACTERISTIC_ID = 'fd65';
let serviceHandle: unknown;
let characteristicHandle: unknown;

const RECEIVE_DATA_HEADER_LENGTH = 5;
const RECEIVE_DATA_LENGTH_OFFSET = 3;

function sendRendererFindBLEDevice(data: string): void {
  BrowserWindow.getFocusedWindow()?.webContents.send('rzNewBLEDevice', data);
}

// function sendRendererDeviceNofify(data: string): void {
//   BrowserWindow.getFocusedWindow()?.webContents.send('rzBLEDeviceNotify', data);
// }

// function sendRendererCommandResult(data: unknown): void {
//   BrowserWindow.getFocusedWindow()?.webContents.send(
//     'rzBLECommandResult',
//     data
//   );
// }

// check if open the bluetooth of host
noble.on('stateChange', (state: unknown) => {
  if (state === 'poweredOn') {
    isBLEOpen = true;

    // notify host
    // not implement
  } else if (state === 'poweredOff') {
    isBLEOpen = false;

    // notify host
    // not implement
  }
});

function startBLEDeviceScan({ filters }): Promise<unknown> {
  return new Promise((resolve, reject) => {
    if (!isBLEOpen) {
      reject(new Error('Please open Bluetooth of PC.'));
      return;
    }

    bleFilters = filters;
    console.log(`startBLEDeviceScan filters: ${filters}`);

    noble.on('discover', (peripheral: unknown) => {
      const { localName } = peripheral.advertisement;

      let isNew = false;
      if (localName && Array.isArray(bleFilters)) {
        console.log(`mac: ${peripheral.address} localName: ${localName}`);

        const len = bleFilters.length;
        if (len === 0) isNew = true;
        for (let i = 0; i < len; i += 1) {
          console.log(
            `${localName} ${bleFilters[i]} ${localName.indexOf(bleFilters[i])}`
          );

          if (localName.indexOf(bleFilters[i]) !== -1) {
            console.log('Find device');

            isNew = true;
            break;
          }
        }

        if (isNew) {
          sendRendererFindBLEDevice(
            JSON.stringify({
              id: peripheral.id,
              name: localName,
              mac: peripheral.address,
              uuid: peripheral.uuid,
              rssi: peripheral.rssi,
              connectable: peripheral.connectable,
            })
          );

          devices.set(peripheral.uuid, peripheral);
        }
      }
    });

    noble.startScanning([], false);
    resolve(true);
  });
}

function stopBLEDeviceScan(): Promise<unknown> {
  return new Promise((resolve, reject) => {
    if (!isBLEOpen) {
      reject(new Error('Please open Bluetooth of PC.'));
      return;
    }

    noble.stopScanning();
    resolve(true);
  });
}

function connectBLEDevice({ deviceId }): Promise<unknown> {
  return new Promise((resolve, reject) => {
    console.log(`connectBLEDevice deviceId: ${deviceId}`);
    if (!devices.has(deviceId)) {
      console.error('No this device');
      reject(new Error('Connected Failed'));
      return;
    }

    serviceHandle = null;
    characteristicHandle = null;

    const peripheral = devices.get(deviceId);
    let timeoutTimer: NodeJS.Timeout;
    peripheral.on('connect', () => {
      console.log('on -> connect');

      clearTimeout(timeoutTimer);
      resolve(true);

      peripheral.updateRssi();
    });

    peripheral.on('disconnect', () => {
      console.log('on -> disconnect');
    });

    peripheral.on('rssiUpdate', (rssi: unknown) => {
      console.log(`on -> RSSI update ${rssi}`);

      peripheral.discoverServices(); // 继续获取service
    });

    peripheral.on('servicesDiscover', (services: unknown) => {
      console.log(`on -> peripheral services discovered ${services}`);

      for (const service of services) {
        if (service.uuid === TARGET_SERVICE_ID) {
          serviceHandle = service;
          break;
        }
      }

      if (serviceHandle) {
        serviceHandle.on(
          'characteristicsDiscover',
          (characteristics: unknown) => {
            for (const charact of characteristics) {
              console.log('charact ', charact.uuid);
              if (charact.uuid === TARGET_CHARACTERISTIC_ID) {
                characteristicHandle = charact;
                break;
              }
            }
          }
        );

        serviceHandle.discoverCharacteristics();
      }
    });

    peripheral.connect();

    timeoutTimer = setTimeout(() => {
      reject(new Error('Connect timeout'));
    }, 5000);
  });
}

function sendBleCommand({ data }): Promise<unknown> {
  return new Promise((resolve, reject) => {
    console.log(`sendBleCommand data: ${data}`);
    if (!characteristicHandle) {
      console.log('Invalid characteristics');
      reject(new Error('Invalid characteristics'));
      return;
    }

    characteristicHandle.on(
      'read',
      (_data: unknown, isNotification: unknown) => {
        console.log(
          `on -> characteristic read: ${_data} isNotification: ${isNotification}`
        );

        const arr = Array.from(_data);
        console.log('arr', arr);
        if (
          isNotification &&
          arr.length > RECEIVE_DATA_HEADER_LENGTH &&
          arr[RECEIVE_DATA_LENGTH_OFFSET] !== RECEIVE_DATA_HEADER_LENGTH
        ) {
          const s = arr.slice(
            RECEIVE_DATA_HEADER_LENGTH,
            arr[RECEIVE_DATA_LENGTH_OFFSET]
          );
          console.log('s', s);

          resolve(s); // success
          // sendRendererCommandResult(s);
        }
      }
    );

    characteristicHandle.on('notify', (state: string) => {
      console.log('on -> characteristic notify ', state);

      characteristicHandle.read();
    });

    // TEST
    // const GET_BLE_NAME_COMMAND = [0xaa, 0, 0, 0x05, 0x86];

    const w = Buffer.from(data);
    console.log('write', w);
    characteristicHandle.notify(true);
    characteristicHandle.write(w, false);
  });
}

export const nobleActionMap = new Map();
nobleActionMap.set('startBLEDeviceScan', startBLEDeviceScan);
nobleActionMap.set('stopBLEDeviceScan', stopBLEDeviceScan);
nobleActionMap.set('connectBLEDevice', connectBLEDevice);
nobleActionMap.set('sendBleCommand', sendBleCommand);
