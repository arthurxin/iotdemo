/* eslint-disable promise/catch-or-return */
/* eslint-disable import/prefer-default-export */
import * as wifi from 'node-wifi';

wifi.init({
  debug: true,
});

function formateStandardResponse(success: boolean, data: unknown) {
  return {
    success,
    data,
  };
}

async function getWifiList(payload: unknown): Promise<unknown> {
  const { filters } = payload;
  return new Promise((resolve, reject) => {
    wifi.scan((err: unknown, networks: unknown) => {
      if (err) {
        reject(formateStandardResponse(false, err));
      } else {
        let res;
        if (filters.length > 0) {
          const len = filters.length;
          res = networks.filter((net: { ssid: string | unknown[] }) => {
            for (let i = 0; i < len; i += 1) {
              if (net.ssid.indexOf(filters[i]) !== -1) return true;
            }
            return false;
          });
        } else {
          res = networks;
        }
        resolve(formateStandardResponse(true, res));
      }
    });
  });
}

function connect(payload: unknown) {
  console.log('connect payload', payload);
  return new Promise((resolve, reject) => {
    wifi.connect(payload).then(
      () => resolve(formateStandardResponse(true, 'Connected')),
      () => reject(formateStandardResponse(false, 'Connect Failed'))
    );
  });
}

export const wifiActionMap = new Map();
wifiActionMap.set('APStartScan', getWifiList);
wifiActionMap.set('APConnect', connect);
