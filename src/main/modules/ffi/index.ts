import { FFIParam, LIBFunc } from '@types';
import * as ffi from 'ffi-napi';

const FILENAME = 'ffiPreload';

const ffiLibMap = new Map();
const ffiCallbackMap = new Map();

function callDLLFunction(
  channel: string,
  libFFI: LIBFunc,
  arg: FFIParam
): string | number | boolean | undefined {
  const FNNAME = `callDLLFunction()`;

  let res: string | number | boolean | undefined;
  if (!libFFI) {
    console.warn(`${FILENAME}.${FNNAME} libFFI:${libFFI}`);
    return;
  }

  console.log(`${FNNAME} action: ${arg.action} arg: ${arg}`);

  try {
    switch (arg.action) {
      case 'SetNodeFFIEvent':
        // for callback, need to handle it separately
        // cannot do it this way as it will be free automatically after a while, need to keep the callback handler
        // res = libFFI.SetNodeFFIEvent(ffi.Callback('void', ['string'], arg.payload.actionArgs));

        if (arg.payload?.actionArgs) {
          const ffiCallback = ffi.Callback(
            'void',
            ['string'],
            arg.payload.actionArgs
          );
          console.log('fficallback', ffiCallback);
          res = libFFI.SetNodeFFIEvent(ffiCallback);
          ffiCallbackMap.set(channel, ffiCallback);
        } else {
          // free the ffi
          res = libFFI.SetNodeFFIEvent(null);
          if (ffiCallbackMap.has(channel)) {
            ffiCallbackMap.delete(channel);
          }
        }
        break;

      case 'future-implememtion':
        break;

      default:
        {
          if (!libFFI[arg.action]) {
            console.warn(
              `${FILENAME}.${FNNAME} action not supported:`,
              arg?.action
            );
            return;
          }

          let r;
          let isFreeMalloc = false;

          try {
            if (arg?.payload?.actionArgs !== undefined) {
              if (Array.isArray(arg.payload.actionArgs)) {
                r = libFFI[arg.action](...arg.payload.actionArgs);
              } else {
                r = libFFI[arg.action](arg.payload.actionArgs);
              }
            } else {
              r = libFFI[arg.action]();
            }
            console.log('r:', r);

            if (Buffer.isBuffer(r)) {
              // need to free it if is returning string
              res = r.readCString();
              console.log('res, ', res);
              isFreeMalloc = true;
            } else {
              res = r;
            }
          } catch (e) {
            console.warn(
              `${FILENAME}.${FNNAME} arg:${JSON.stringify(arg)}, e:${e.message}`
            );
          } finally {
            if (isFreeMalloc) {
              if (r && libFFI.FreeMalloc) {
                console.log('free memory');
                libFFI.FreeMalloc(r);
              } else {
                console.warn(
                  `${FILENAME}.${FNNAME} arg:${JSON.stringify(
                    arg
                  )}, missing freeMalloc`
                );
              }
            }
          }

          if (arg.action === 'Terminate') {
            ffiLibMap.delete(channel);
            ffiCallbackMap.delete(channel);
          }
        }
        break;
    }
  } catch (e) {
    console.warn(
      `${FILENAME}.${FNNAME} arg:${JSON.stringify(arg)}, e:${e.message}`
    );
  }

  // eslint-disable-next-line consistent-return
  return res;
}

async function callDLLFunctionAsync(
  channel: string,
  libFFI: LIBFunc,
  arg: FFIParam
) {
  const FNNAME = `callDLLFunctionAsync()`;

  let res;
  if (!libFFI) {
    console.warn(`${FILENAME}.${FNNAME} libFFI:${libFFI}`);
    return;
  }

  try {
    switch (arg.action) {
      case 'SetNodeFFIEvent':
        // for callback, need to handle it separately
        // cannot do it this way as it will be free automatically after a while, need to keep the callback handler
        // res = libFFI.SetNodeFFIEvent(ffi.Callback('void', ['string'], arg.payload.actionArgs));

        if (arg.payload.actionArgs) {
          const ffiCallback = ffi.Callback(
            'void',
            ['string'],
            arg.payload.actionArgs
          );
          res = libFFI.SetNodeFFIEvent(ffiCallback);
          ffiCallbackMap.set(channel, ffiCallback);
        } else {
          // free the ffi
          res = libFFI.SetNodeFFIEvent(null);
          if (ffiCallbackMap.has(channel)) {
            ffiCallbackMap.delete(channel);
          }
        }
        break;

      case 'future-implememtion':
        break;

      default:
        {
          if (!libFFI[arg.action]) {
            console.warn(
              `${FILENAME}.${FNNAME} action not supported:`,
              arg?.action
            );
            return;
          }

          let r;
          let isFreeMalloc = false;

          try {
            if (arg?.payload?.actionArgs !== undefined) {
              if (Array.isArray(arg.payload.actionArgs)) {
                r = await new Promise((resolve) => {
                  libFFI[arg.action].async(
                    ...arg.payload.actionArgs,
                    (err, r1) => {
                      resolve(r1);
                    }
                  );
                });
              } else {
                r = await new Promise((resolve) => {
                  libFFI[arg.action].async(
                    arg.payload.actionArgs,
                    (err, r1) => {
                      resolve(r1);
                    }
                  );
                });
              }
            } else {
              r = await new Promise((resolve) => {
                libFFI[arg.action].async((err, r1) => {
                  resolve(r1);
                });
              });
            }

            if (Buffer.isBuffer(r)) {
              // need to free it if is returning string
              res = r.readCString();
              isFreeMalloc = true;
            } else {
              res = r;
            }
          } catch (e) {
            console.warn(
              `${FILENAME}.${FNNAME} arg:${JSON.stringify(arg)}, e:${e.message}`
            );
          } finally {
            if (isFreeMalloc) {
              if (r && libFFI.FreeMalloc) {
                console.log('free memory');
                libFFI.FreeMalloc(r);
              } else {
                console.warn(
                  `${FILENAME}.${FNNAME} arg:${JSON.stringify(
                    arg
                  )}, missing freeMalloc`
                );
              }
            }
          }
        }

        if (arg.action === 'Terminate') {
          ffiLibMap.delete(channel);
          ffiCallbackMap.delete(channel);
        }

        break;
    }
  } catch (e) {
    console.warn(
      `${FILENAME}.${FNNAME} arg:${JSON.stringify(arg)}, e:${e.message}`
    );
  }

  console.log(
    `${FILENAME}.${FNNAME} channel:${channel}, arg:${JSON.stringify(
      arg
    )}, res:${JSON.stringify(res)}`
  );

  // eslint-disable-next-line consistent-return
  return res;
}

export const callDLL = (channel: string, arg: FFIParam) => {
  const FNNAME = `callDLL()`;
  console.log(`${FILENAME}.${FNNAME} arg:${arg}`);

  let r;
  try {
    switch (arg?.action) {
      case 'ConfigureFFI':
        {
          if (ffiLibMap.get(channel)) {
            console.warn(
              `${FILENAME}.${FNNAME} arg:${JSON.stringify(
                arg
              )}, r:${JSON.stringify(r)}, ffi handler already exists`
            );
            r = false;
            return r;
          }

          const libFFI = ffi.Library(arg.payload?.dllPath, arg.payload?.apiObj);
          console.log(`${FILENAME}.${FNNAME} libFFI:`, libFFI);

          if (libFFI) {
            ffiLibMap.set(channel, libFFI);
            r = true;
          }
        }
        break;
      case 'FreeFFI':
        if (!ffiLibMap.has(channel)) {
          console.warn(
            `${FILENAME}.${FNNAME} arg:${JSON.stringify(
              arg
            )}, r:${JSON.stringify(r)}, missing ffi handler`
          );
          return r;
        }

        ffiLibMap.delete(channel);
        break;

      default:
        {
          if (!ffiLibMap.has(channel)) {
            console.warn(
              `${FILENAME}.${FNNAME} arg:${JSON.stringify(
                arg
              )}, r:${JSON.stringify(r)}, missing ffi handler`
            );
            return r;
          }

          const libFFI = ffiLibMap.get(channel);
          r = callDLLFunction(channel, libFFI, arg);
        }
        break;
    }
    // console.log(`${FILENAME}.${FNNAME} arg:${JSON.stringify(arg)}, r:${JSON.stringify(r)}`);
  } catch (e) {
    console.warn(
      `${FILENAME}.${FNNAME} arg:${JSON.stringify(arg)}, e:${
        e.message
      } r:${JSON.stringify(r)}`
    );
  }

  return r;
};

export const callDLLAsync = async (channel: string, arg: FFIParam) => {
  const FNNAME = `callDLLAsync()`;
  let r;

  try {
    switch (arg?.action) {
      case 'ConfigureFFI':
        {
          if (ffiLibMap.get(channel)) {
            console.warn(
              `${FILENAME}.${FNNAME} arg:${JSON.stringify(
                arg
              )}, r:${JSON.stringify(r)}, ffi handler already exists`
            );
            r = false;
            return r;
          }

          const libFFI = ffi.Library(arg.payload?.dllPath, arg.payload?.apiObj);
          console.log(`${FILENAME}.${FNNAME} libFFI:`, libFFI);

          if (libFFI) {
            ffiLibMap.set(channel, libFFI);
            r = true;
          }
        }
        break;

      case 'FreeFFI':
        if (!ffiLibMap.has(channel)) {
          console.warn(
            `${FILENAME}.${FNNAME} arg:${JSON.stringify(
              arg
            )}, r:${JSON.stringify(r)}, missing ffi handler`
          );
          return r;
        }

        ffiLibMap.delete(channel);
        break;

      default:
        {
          if (!ffiLibMap.has(channel)) {
            console.warn(
              `${FILENAME}.${FNNAME} arg:${JSON.stringify(
                arg
              )}, r:${JSON.stringify(r)}, missing ffi handler`
            );
            return r;
          }

          const libFFI = ffiLibMap.get(channel);
          r = await callDLLFunctionAsync(channel, libFFI, arg);
        }
        break;
    }
    console.log(
      `${FILENAME}.${FNNAME} arg:${JSON.stringify(arg)}, r:${JSON.stringify(r)}`
    );
  } catch (e) {
    console.warn(
      `${FILENAME}.${FNNAME} arg:${JSON.stringify(arg)}, e:${e.message}`
    );
  }

  return r;
};
